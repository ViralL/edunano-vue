import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    auth: {
      user: null,
      loggedIn: false,
    },
    ids: [],
    learning_id: null,
    education_id: null,
    forum_id: null,
    token: null,
  },
  mutations: {
    authorization(state, data) {
      state.token = data.token;
      state.auth.user = data.user;
      state.auth.loggedIn = true;
      state.learning_id = data.learning_id;
      state.education_id = data.education_id;
      state.forum_id = data.forum_id;
    },
    setIds(state, data) {
      state.ids = data.data;
    },
  },
  actions: {
    authorizationAction(context, { options }) {
      store.commit('authorization', {
        // token: options.token,
        user: options,
        learning_id: options.learning_id,
        education_id: options.id,
        forum_id: options.ppk_learning.forum_id,
      });
    },
    setIds(context, { data }) {
      store.commit('setIds', {
        ids: data,
      });
    },
    logout(state) {
      state.auth.user = false;
      state.auth.loggedIn = false;
    },
  },
  modules: {
  }
})

export default store;
