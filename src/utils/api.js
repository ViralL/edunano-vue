import axios from 'axios';

const sApiURL =
    process.env.NODE_ENV === 'development'
        ? "https://nextwt.edunano.ru/eNano/api/v2/api.html"
        : '/eNano/api/v2/api.html';
const oAuthData = null;
const withCredentials = true;


export default {
    methods: {
        getSupportChat(type = "sysadmin") {
            let params = {
                type
            };

            return axios.get(sApiURL + "?ns=chat&method=getSupportChat", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        processChat(chat_id, message, last_message) {
            let params = {
                chat_id,
                message,
                last_message
            };

            return axios.post(sApiURL + "?ns=chat&method=processChat",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        createSupportChat() {
            let params = {};

            return axios.post(sApiURL + "?ns=chat&method=createSupportChat",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        addActivityJournalRecord(education_id = "6933168803424728394", record_type = "last-visit", event_id) {
            let params = {
                education_id,
                record_type,
                event_id,
            };

            return axios.post(sApiURL + "?ns=mooc&method=addActivityJournalRecord",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        saveCurrentPath(education_id = "6933168803424728394", path = location.pathname + location.search) {
            let params = {
                education_id,
                path,
            };

            if (education_id) {
                return true;
            } else {
                return axios.post(sApiURL + "?ns=mooc&method=saveCurrentPath",
                    params,
                    {auth: oAuthData, withCredentials: withCredentials},
                );
            }
        },
        getEducationPlan(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getEducationPlan", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getEducation(learning_id = "6933168796138803821") { // 6838661957151311046
            let params = {
                learning_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getEducation", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        exportLearningStudents(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=exportLearningStudents", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getCertificate(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getCertificate", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        changeActivityPassingStatus(education_id = "6933168803424728394", activity_id = "6607720857498306482", status = "passed", parent_activity_id = null) {
            let params = {
                education_id,
                activity_id,
                status,
                parent_activity_id,
            };

            return axios.post(sApiURL + "?ns=mooc&method=changeActivityPassingStatus",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        getEducationModule(education_id, module_id) {
            let params = {
                education_id,
                module_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getEducationModule", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        recordStatement(education_id = "6933168803424728394", activity_id = "6607720857498306482", statements = [{name: "view_time", value: "9101"}]) {
            let params = {
                education_id,
                activity_id,
                statements,
            };

            return axios.post(sApiURL + "?ns=mooc&method=recordStatement",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        getCalendarEvents(education_id = "6933168803424728394", start = "2020-06-24", end = "2022-06-25") {
            let params = {
                education_id,
                start,
                end,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getCalendarEvents", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getGoogleCalendarEvents(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getGoogleCalendarEvents", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getEducationMaterials(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getEducationMaterials", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getUsefulLinks(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getUsefulLinks", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        webinarActivity(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=WebinarActivity", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getLearningStudents(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getLearningStudents", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        setStudentSharedFields(education_id = "6933168803424728394", shared_fields = ["fio", "age", "birthday"]) {
            let params = {
                education_id,
                shared_fields,
            };

            return axios.post(sApiURL + "?ns=mooc&method=setStudentSharedFields",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        getEducationProgress(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getEducationProgress", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getPolls(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getPolls", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getPoll(education_id = "6933168803424728394", id = "7034606937978323906") {
            let params = {
                education_id,
                id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getPoll", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getTopics(education_id = "6933168803424728394", forum_id = "6933168811782518169") {
            let params = {
                education_id,
                forum_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getTopics", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        getMessages(education_id = "6933168803424728394", forum_entry_id = "7034530565515910239") {
            let params = {
                education_id,
                forum_entry_id
            };

            return axios.get(sApiURL + "?ns=mooc&method=getMessages", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        savePoll(education_id = "6933168803424728394", id = "6933168803424728394", answers) {
            let params = {
                education_id,
                id,
                answers
            };

            return axios.post(sApiURL + "?ns=mooc&method=savePoll",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        readTopic(education_id = "6933168803424728394", id = "6933168803424728394") {
            let params = {
                education_id,
                id
            };

            return axios.post(sApiURL + "?ns=mooc&method=readTopic",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        deleteMessage(education_id = "6933168803424728394", id = "6933168803424728394") {
            let params = {
                education_id,
                id
            };

            return axios.post(sApiURL + "?ns=mooc&method=deleteMessage",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        postMessage(education_id, forum_id, id, parent_id, name, text_area, files, excluded_files) {
            let fd = new FormData();
            fd.append("education_id", education_id);
            fd.append("forum_id", forum_id);
            fd.append("name", name);
            fd.append("text_area", text_area);
            if (id) fd.append("id", id);
            if (id) fd.append("parent_id", parent_id);
            if (excluded_files) fd.append("excluded_files", excluded_files);

            if (files.length) {
                const a = Array.from(files);
                a.map((item, index) => {
                    fd.append(`file${index}`, item, item.name);
                })
            }

            return axios.post(sApiURL + "?ns=mooc&method=postMessage",
                fd,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        getNotifications(education_id = "6933168803424728394") {
            let params = {
                education_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getNotifications", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        setViewed(education_id = "6933168803424728394", type = 'notif', object_id) {
            let params = {
                education_id,
                type,
                object_id
            };

            return axios.post(sApiURL + "?ns=mooc&method=setViewed",
                params,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        getTaskSolution(education_id = "6933168803424728394", solution_id = "6933168804022663287") {
            let params = {
                education_id,
                solution_id,
            };

            return axios.get(sApiURL + "?ns=mooc&method=getTaskSolution", {
                params: params,
                auth: oAuthData,
                withCredentials: withCredentials,
            });
        },
        commentTaskSolution(education_id = "6933168803424728394", solution_id = "6933168804022663287", text, files) {
            let fd = new FormData();
            fd.append("education_id", education_id);
            fd.append("solution_id", solution_id);
            fd.append("text", text);

            if (files.length) {
                const a = Array.from(files)
                a.map((item, index) => {
                    fd.append(`file${index}`, item, item.name);
                })
            }

            return axios.post(sApiURL + "?ns=mooc&method=commentTaskSolution",
                fd,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
        acceptTaskSolution(education_id = "6933168803424728394", solution_id = "6933168804022663287", text, mark, files) {
            let fd = new FormData();
            fd.append("education_id", education_id);
            fd.append("solution_id", solution_id);
            fd.append("text", text);
            fd.append("mark", mark);

            if (files.length) {
                const a = Array.from(files)
                a.map((item, index) => {
                    fd.append(`file${index}`, item, item.name);
                })
            }

            return axios.post(sApiURL + "?ns=mooc&method=acceptTaskSolution",
                fd,
                {auth: oAuthData, withCredentials: withCredentials},
            );
        },
    },
}
