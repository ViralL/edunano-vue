export default {
  methods: {
    countRow(task, webinar, tests) {
      let count = 0;
      if (task) count++;
      if (webinar) count++;
      if (tests) count++;

      return count;
    },
    setClasses(count) {
      let classes = '';

      if (count === 1) {
        classes = 'col col__xs-12';
      }

      if (count === 2) {
        classes = 'col col__md-6 col__xs-12';
      }

      if (count === 3) {
        classes = 'col col__lg-4 col__md-6 col__xs-12';
      }

      return classes;
    },
    numberWithSpaces(x) {
      if (!x) return x
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
    },
    reloadPage(){
      window.location.reload()
    },
    get(obj, path, defaultValue = undefined) {
      const travel = (regexp) => String.prototype.split
        .call(path, regexp)
        .filter(Boolean)
        .reduce((res, key) => (res !== null && res !== undefined ? res[key] : res), obj);
      const result = travel(/[,[\]]+?/) || travel(/[,[\].]+?/);
      return result === undefined || result === obj ? defaultValue : result;
    },
    setDays(screenWidth) {
      let days;
      if (screenWidth > 991 && screenWidth < 1100) {
        days = 4;
      } else if (screenWidth > 767 && screenWidth < 992) {
        days = 3;
      } else if (screenWidth < 768) {
        days = 1;
      } else {
        days = 7;
      }

      return days;
    },
    getUniqueStatus(arr) {
      if(!arr) {
        return;
      }
      let obj = [];
      arr.find(o => {
        if(o.status === '') {
          obj.push(o);
        }
      });

      return obj;
    },
    scrollTo(el, offset = 0) {
      if (!el) return;
      window.scrollTo({
        top: window.scrollY + el.getBoundingClientRect().y - offset,
        behavior: 'smooth',
      });
    },
  },
};
