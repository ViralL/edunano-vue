import moment from 'moment';

export default {
  methods: {
    formatDate(string) {
      if (!string) {
        return;
      }
      return moment(string).format('H:mm DD.MM.YYYY');
    },
    formatDateNoTime(string) {
      if (!string) {
        return;
      }
      return moment(string, 'YYYY-MM-DDTH:mm:ss').format('DD.MM.YYYY');
    },
    formatDateStart(string) {
      if (!string) {
        return;
      }
      return moment(string, 'YYYY-MM-DDTH:mm:ss').format('DD.MM.YYYY H:mm');
    },
    formatDateEnd(string) {
      if (!string) {
        return;
      }
      return moment(string, 'YYYY-MM-DDTH:mm:ss').format('H:mm');
    },
    calcDate(string) {
      if (!string) {
        return;
      }
      return moment(string, 'YYYY-MM-DDTH:mm:ss').format('YYYY-MM-DD');
    },
  },
};
