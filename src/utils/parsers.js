export default {
  methods: {
    isUser(fullname, person) {
      const setFullName = `${person.lastname} ${person.firstname} ${person.middlename}`;
      if (fullname === setFullName) {
        return true;
      }
      return false;
    },
    getTypeFromArray(arr) {
      let result = 'course';
      arr.map(item => {
        if (item.type === 'Практическое задание') {
          result = 'practice';
        }
        if (item.type === 'Вебинар') {
          result = 'video';
        }
        if (item.type === 'Метериал') {
          result = 'materials';
        }
        if (item.type === 'Тест') {
          result = 'tests';
        }
      })

      return result;
    },
    getSolutionId(arr, id) {
      let solution_id = id;
      arr.map(item => {
        if (item.type === 'Практическое задание' && item.id === id) {
          solution_id = item.task?.solution_id;
        }
      })

      return solution_id;
    },
    getType(x) {
      let icon;
      switch (x) {
        case 'Tilda':
          icon = 'icon-video';
          break;
        case 'Вебинар':
          icon = 'icon-webinar';
          break;
        case 'Видео':
          icon = 'icon-video';
          break;
        case 'Групповая работа':
          icon = 'icon-video';
          break;
        case 'Метериал':
          icon = 'icon-materials';
          break;
        case 'Несколько видео':
          icon = 'icon-video';
          break;
        case 'Практическое задание':
          icon = 'icon-video';
          break;
        case 'Тест':
          icon = 'icon-test';
          break;
        default:
          icon = 'icon-video';
      }

      return icon;
    },
    getStatus(x) {
      let status;
      switch (x) {
        case 'passed':
          status = 'eduProgress-list-el--finished';
          break;
        case 'failed':
          status = 'eduProgress-list-el--failed';
          break;
        case 'in_process':
          status = '';
          break;
        default:
          status = '';
      }
      return status;
    },
    getStatusIcon(x) {
      let status;
      switch (x) {
        case 'passed':
          status = 'checked--finished';
          break;
        case 'failed':
          status = 'checked--failed';
          break;
        case 'in_process':
          status = 'checked--awaiting';
          break;
        default:
          status = '';
      }
      return status;
    },
    parseDate(date) {
      const result = [];
      date.forEach(item => {
        const detectDays = this.calcDate(item.start) === this.calcDate(item.end);
        result.push({
          start: detectDays ? item.start : this.calcDate(item.start),
          end: detectDays ? item.end : this.calcDate(item.end),
          titles: item.titles,
          ico: item.type === 'Вебинар' ? this.video : this.doc,
          dates: `${this.formatDateStart(item.start)} - ${this.formatDateStart(item.end)} MSK`,
        })
      })

      return result;
    }
  },
};
