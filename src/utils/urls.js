const base = '/view_doc.html?mode=mooc&al_id=6933168796138803821';

const Urls = {
  program: base+'&tab=program',
  course: base+'&tab=course',
  calendar: base+'&tab=calendar',
  users: base+'&tab=users',
  progress: base+'&tab=progress',
  forum: base+'&tab=forum',
  forumEdit: base+'&tab=forum-edit',
  forumLast: base+'&tab=forum-last',
  practice: base+'&tab=practice',
  tests: base+'&tab=tests',
  advertisement: base+'&tab=advertisement',
  questionnaire: base+'&tab=questionnaire',
  worksheet: base+'&tab=worksheet',
  helper: base+'&tab=helper',
  materials: base+'&tab=materials',
  video: base+'&tab=video',
  account: '/view_doc.html?mode=my_account',
};
Urls.install = function (Vue) {
  Vue.prototype.$getConst = (key) => {
    return Urls[key];
  }
};
export default Urls;
