const base = '/view_doc.html?mode=mooc';
const cabinet = '/view_doc.html?mode=example';


export default {
  URLS: {
    program: base+'&tab=program',
    course: base+'&tab=course/:id',
    calendar: base+'&tab=calendar',
    users: base+'&tab=users',
    progress: base+'&tab=progress',
    forum: base+'&tab=forum',
    materials: base+'&tab=materials',
    video: base+'&tab=video',
    tests: base+'&tab=tests',
    forumLast: base+'&tab=forum-last',
    forumEdit: base+'&tab=forum-edit',
    questionnaire: base+'&tab=questionnaire',
    worksheet: base+'&tab=worksheet/:id',
    cabinet: cabinet,
  }
}

