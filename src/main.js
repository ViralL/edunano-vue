import Vue from 'vue'
import App from './App.vue'
import AppSide from './AppSide.vue'
import router from './router'
import store from './store/index'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import JwPagination from 'jw-vue-pagination'
import VueSelect from "vue-select"
import UrlsConstants from "./utils/urls"

import './assets/styles/_app.scss'
import "vue-select/dist/vue-select.css"
import 'swiper/swiper-bundle.css'

Vue.use(UrlsConstants);
Vue.use(VueAwesomeSwiper)
Vue.component('jw-pagination', JwPagination);
Vue.component("v-select", VueSelect);

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(AppSide)
}).$mount('#sidebar')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#main')
