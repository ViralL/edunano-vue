import Vue from 'vue'
import VueRouter from 'vue-router'
import App from "@/App";

Vue.use(VueRouter)


const routes = [
  {
    // path: '/view_doc.html?mode=mooc&al_id=6933168796138803821&tab=program',
    path: '*',
    name: 'App',
    component: App,
  }
]

const router = new VueRouter({mode: 'history', routes: routes})

export default router
